<?php

/**
 * @file
 * This is a Feeds plugin that will fetch file from an URL.
 */

/**
 * Result of ShopifyFetcher::fetch().
 */
class ShopifyFetcherResult extends FeedsHTTPFetcherResult {
  protected $url;

  /**
   * Constructor.
   */
  public function __construct($url = NULL) {
    $this->url = $url;

    parent::__construct($url);
  }

  /**
   * Overrides FeedsFetcherResult::getRaw();
   */
  public function getRaw() {
    feeds_include_library('http_request.inc', 'http_request');
    $result = http_request_get($this->url, NULL, NULL, NULL, $this->timeout);
    if (!in_array($result->code, array(200, 201, 202, 203, 204, 205, 206))) {
      throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $result->code)));
    }

    
    return $this->sanitizeRaw($result->data);
  }

}

/**
 * Fetches data via URL.
 */
class ShopifyFetcher extends FeedsFetcher {
  protected $shopify_apikey, $shopify_password, $shopify_shop_url;

  // public function __construct(){
  //   parent::__construct();
  //   $this->getShopifyVariables();
  // }

  public function getShopifyVariables(){
    $this->shopify_apikey = variable_get("shopify_apikey");
    $this->shopify_password = variable_get("shopify_password");
    $this->shopify_shop_url = variable_get("shopify_shop_url");
  }

  public function validateShopifyConfig(){
    if($this->shopify_apikey && $this->shopify_password && $this->shopify_shop_url):
      return TRUE;
    else:
      return FALSE;
    endif;
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    $this->getShopifyVariables();

    return array(
      'request_timeout' => NULL,
      'shopify_password' => $this->shopify_password,
      'shopify_apikey' => $this->shopify_apikey,
      'shopify_shop_url' => $this->shopify_shop_url,
      'shopify_api_type' => 'products'
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {

    $form = array();

    //if($this->validateShopifyConfig() ):
      $form['example_url'] = array(
      '#type' => 'item',
      '#title' => t('Example Shopify API URL'),
      '#description' => t('Here is an example URL.'),
      '#markup' => "<a target='_blank' href='https://{$this->shopify_apikey}:{$this->shopify_password}@{$this->shopify_shop_url}/admin/".$this->config['shopify_api_type'].".json'>https://{$this->shopify_apikey}:{$this->shopify_password}@{$this->shopify_shop_url}/admin/".$this->config['shopify_api_type'].".json</a>",
    );

    $form['shopify_api_type'] = array(
      '#type' => 'select',
      '#title' => t('Shopify API Type'),
      '#description' => t('The Shopify API method to Call.'),
      '#default_value' => $this->config['shopify_api_type'],
      '#required' => TRUE,
      '#options' => array(
        'products' => t('Products'),
        'variants' => t('Variants'),
        'orders' => t('Orders'),
        'customers' => t('Customers'),
      ),
    );
    
   // endif;

   //  $form['auto_detect_feeds'] = array(
   //    '#type' => 'checkbox',
   //    '#title' => t('Auto detect feeds'),
   //    '#description' => t('If the supplied URL does not point to a feed but an HTML document, attempt to extract a feed URL from the document.'),
   //    '#default_value' => $this->config['auto_detect_feeds'],
   //  );

   //  $form['auto_detect_feeds'] = array(
   //    '#type' => 'checkbox',
   //    '#title' => t('Auto detect feeds'),
   //    '#description' => t('If the supplied URL does not point to a feed but an HTML document, attempt to extract a feed URL from the document.'),
   //    '#default_value' => $this->config['auto_detect_feeds'],
   //  );
   //  $form['use_pubsubhubbub'] = array(
   //    '#type' => 'checkbox',
   //    '#title' => t('Use PubSubHubbub'),
   //    '#description' => t('Attempt to use a <a href="http://en.wikipedia.org/wiki/PubSubHubbub">PubSubHubbub</a> subscription if available.'),
   //    '#default_value' => $this->config['use_pubsubhubbub'],
   //  );
   //  $form['designated_hub'] = array(
   //    '#type' => 'textfield',
   //    '#title' => t('Designated hub'),
   //    '#description' => t('Enter the URL of a designated PubSubHubbub hub (e. g. superfeedr.com). If given, this hub will be used instead of the hub specified in the actual feed.'),
   //    '#default_value' => $this->config['designated_hub'],
   //    '#dependency' => array(
   //      'edit-use-pubsubhubbub' => array(1),
   //    ),
   //  );
   // // Per importer override of global http request timeout setting.
   // $form['request_timeout'] = array(
   //   '#type' => 'textfield',
   //   '#title' => t('Request timeout'),
   //   '#description' => t('Timeout in seconds to wait for an HTTP get request to finish.</br>' .
   //                       '<b>Note:</b> this setting will override the global setting.</br>' .
   //                       'When left empty, the global value is used.'),
   //   '#default_value' => $this->config['request_timeout'],
   //   '#element_validate' => array('element_validate_integer_positive'),
   //   '#maxlength' => 3,
   //   '#size'=> 30,
   // );
    return $form;
  }

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    
    if ($this->config['use_pubsubhubbub'] && ($raw = $this->subscriber($source->feed_nid)->receive())) {
      return new ShopifyFetcherResult($raw);
    }

    $state = $source->state(FEEDS_FETCH);

    feeds_include_library('http_request.inc', 'http_request');

    if (empty($state->inited)) {
      $state->inited = TRUE;
      // $response = http_request_get($source_config['source']);
      // if ($response->code != 200) {
      //   throw new Exception();
      // }
      // $state->urls = $this->parseURLs($response->data, $source_config);

      $state->urls = array();
      $max_pages = 100;
      for($page = 1; $page < $max_pages; $page++){
        $state->urls[] = "https://{$this->shopify_apikey}:{$this->shopify_password}@{$this->shopify_shop_url}/admin/".$source_config['shopify_api_type'].".json?limit=250&page={$page}";
      }

      $state->total = count($state->urls);
    }

    if (empty($state->urls)) {
      new FeedsHTTPFetcherResult('');
    }

    $url = array_shift($state->urls);

    $state->progress($state->total, $state->total - count($state->urls));

    return new ShopifyFetcherResult($url);
    //return new ShopifyFetcherResult($url);
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $shopify_apikey = variable_get("shopify_apikey");
    $shopify_password = variable_get("shopify_password");
    $shopify_shop_url = variable_get("shopify_shop_url");



    $form = array();

    $form['shopify_api_type'] = array(
      '#type' => 'select',
      '#title' => t('Shopify API Type'),
      '#description' => t('The Shopify API method to Call.'),
      '#default_value' => isset($source_config['shopify_api_type']) ? $source_config['shopify_api_type'] : '',
      '#required' => TRUE,
      '#options' => array(
        'products' => t('Products'),
        'variants' => t('Variants'),
        'orders' => t('Orders'),
      ),
    );


    return $form;
  }

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    $values['shopify_api_type'] = trim($values['shopify_api_type']);

    $shopify_apikey = variable_get("shopify_apikey");
    $shopify_password = variable_get("shopify_password");
    $shopify_shop_url = variable_get("shopify_shop_url");

//    var_dump($shopify_apikey);
      //form_set_error($form_key, t('The URL %source is invalid.', array('%source' => $values['source'])));

    if (! $values['shopify_api_type'] ) {
      $form_key = 'feeds][' . get_class($this) . '][shopify_api_type';
      form_set_error($form_key, t('The URL %shopify_api_type is invalid.', array('%shopify_api_type' => $values['shopify_api_type'])));
    }
    // elseif ($this->config['auto_detect_feeds']) {
    //   feeds_include_library('http_request.inc', 'http_request');
    //   if ($url = http_request_get_common_syndication($values['source'])) {
    //     $values['source'] = $url;
    //   }
    // }
  }
}